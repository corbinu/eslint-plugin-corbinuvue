import Vue3Rc from "./vue3rc";
import VueRc from "./vuerc";

export = {
    configs: {
        vue: VueRc,
        vue3: Vue3Rc,
    },
};
