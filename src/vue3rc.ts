import VueRc from "./vuerc";

VueRc.extends = ["plugin:vue/vue3-recommended", "plugin:@corbinu/corbinu/base", "prettier/vue"];

export default VueRc;
