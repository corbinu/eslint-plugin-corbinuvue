const vuerc = {
    root: true,
    parser: "vue-eslint-parser",
    parserOptions: {
        parser: "@typescript-eslint/parser",
        project: ["./tsconfig.lint.json"],
        extraFileExtensions: [".vue", ".json"],
        ecmaFeatures: {
            jsx: true,
        },
    },
    env: {
        node: true,
    },
    plugins: ["@corbinu/corbinu"],
    extends: ["plugin:vue/recommended", "plugin:@corbinu/corbinu/base", "plugin:@corbinu/corbinu/jest", "prettier/vue"],
    rules: {
        "no-console": "off",
        "no-debugger": "off",
        "@typescript-eslint/no-unused-vars-experimental": "off",
        "@typescript-eslint/no-unused-vars": "error",
        "@typescript-eslint/explicit-function-return-type": "off",
        "vue/component-tags-order": [
            "error",
            {
                order: ["template", "script", "style"],
            },
        ],
    },
    overrides: [
        {
            files: ["shims-tsx.d.ts"],
            rules: {
                "@typescript-eslint/no-empty-interface": "off",
                "@typescript-eslint/no-explicit-any": "off",
                "@typescript-eslint/no-unused-vars": "off",
                "@typescript-eslint/no-type-alias": "off",
            },
        },
        {
            files: ["vue.config.js"],
            rules: {
                "@typescript-eslint/no-require-imports": "off",
                "@typescript-eslint/no-var-requires": "off",
            },
        },
    ],
};

if (process.env.NODE_ENV === "production") {
    vuerc.rules["no-console"] = "error";
    vuerc.rules["no-debugger"] = "error";
}

export default vuerc;
